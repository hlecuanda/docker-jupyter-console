<img src="./JupyterConsole.png" width="200px">

# A python 3.7 development environment

- using `conda` to manage packages and dependencies
- based on *alpine linux* so it's as light as it can be
- `jupyter-console` and `qtconsole` preinstalled
- No nonsense `Makefile` automation
- libraries for development on google cloud environments

